import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalService } from '../../services/modals/modal.service';
import { FirebaseService } from '../../services/firebase/firebase.service';
import { Ticket } from '../../components/ticket/ticket';
import { CdkDragDrop, transferArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material/dialog';
import { CreateTicketModalComponent, TicketDialogResult } from 'src/app/components/modals/create-ticket-modal/create-ticket-modal.component';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { BehaviorSubject } from 'rxjs';


@Component({
  selector: 'app-desk-page',
  templateUrl: './desk-page.component.html',
  styleUrls: ['./desk-page.component.scss']
})
export class DeskPageComponent implements OnInit {

  getObservable = (collection: AngularFirestoreCollection<Ticket>) => {
    const subject = new BehaviorSubject([] as any);
    collection.valueChanges({ idField: 'id' }).subscribe((val: Ticket[]) => {
      subject.next(val);
      this.loading = false;
    });
    return subject;
  }

  loading = true;
  deskId = this.router.url.split('').reverse().join('').split('', 9).reverse().join('');
  todo = this.getObservable(this.store.collection('todo'));
  inProgress = this.getObservable(this.store.collection('inProgress'));
  done = this.getObservable(this.store.collection('done'));

  constructor(
    public modalService: ModalService,
    public firebaseService: FirebaseService,
    private dialog: MatDialog,
    private store: AngularFirestore,
    private router: Router
  ) {
    this.modalService.showCreateDeskModal = false;
    this.modalService.mainPage = false;
    this.modalService.teamsPage = true;
  }

  ngOnInit(): void {
    this.firebaseService.checkUserDesk(this.deskId);
  }

  drop(event: CdkDragDrop<Ticket[]>): void {
    const item = event.previousContainer.data[event.previousIndex];
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      this.store.firestore.runTransaction(() => {
        const promise = Promise.all([
          this.store.collection(event.previousContainer.id).doc(item.id).delete(),
          this.store.collection(event.container.id).add(item),
        ]);
        return promise;
      });
    }
  }

  newTask(): void {
    this.modalService.showCreateTicketModal = true;
    setTimeout(() => {
      const dialogRef = this.dialog.open(CreateTicketModalComponent, {
        width: '270px',
        data: {
          ticket: {
            deskId: this.deskId
          },
        },
      });
      dialogRef
        .afterClosed()
        .subscribe((result: TicketDialogResult) => {
          // tslint:disable-next-line: no-unused-expression
          (result && (result.ticket.description || result.ticket.title)) && this.store.collection('todo').add(result.ticket);
        });
    });
  }

  editTask(list: 'done' | 'todo' | 'inProgress', ticket: Ticket): void {
    const dialogRef = this.dialog.open(CreateTicketModalComponent, {
      width: '270px',
      data: {
        ticket,
        enableDelete: true,
      },
    });
    dialogRef.afterClosed().subscribe((result: TicketDialogResult) => {
      if (result && result.delete) {
        this.store.collection(list).doc(ticket.id).delete();
      } else {
        this.store.collection(list).doc(ticket.id).update(ticket);
      }
    });
  }
}
