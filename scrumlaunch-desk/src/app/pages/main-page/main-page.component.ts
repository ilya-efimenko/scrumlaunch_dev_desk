import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalService } from '../../services/modals/modal.service';
import { FirebaseService } from '../../services/firebase/firebase.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  deskBackground: any;
  deskId: any;

  constructor(
    private router: Router,
    public modalService: ModalService,
    public firebaseService: FirebaseService,
  ) {
    this.modalService.showProfileModal = false;
    this.modalService.showCreateDeskModal = false;
    this.modalService.showSearchModal = false;
    this.modalService.mainPage = true;
    this.modalService.teamsPage = true;
  }

  ngOnInit(): void {
    this.firebaseService.getCurrentUser();
    this.firebaseService.getAllUsers();
    this.firebaseService.data.userDesks = [];
  }

  goToDesk(id: string): void {
    this.router.navigate([`/desk/${id}`]);
  }

}
