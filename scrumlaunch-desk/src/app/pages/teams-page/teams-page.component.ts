import { Component, OnInit, ViewChild, TemplateRef, EventEmitter } from '@angular/core';
import { ModalService } from '../../services/modals/modal.service';
import { FirebaseService } from '../../services/firebase/firebase.service';

@Component({
  selector: 'app-teams-page',
  templateUrl: './teams-page.component.html',
  styleUrls: ['./teams-page.component.scss']
})
export class TeamsPageComponent implements OnInit {

  @ViewChild('members') members: TemplateRef<any> | undefined;
  @ViewChild('groups') groups: TemplateRef<any> | undefined;
  selected: any;

  constructor(
    public modalService: ModalService,
    public firebaseService: FirebaseService
  ) {
    this.modalService.teamsPage = false;
    this.firebaseService.getTeams();
  }

  ngOnInit(): void {
  }

  selectTab(option: any) {
    this.selected = option;
  }

}
