import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../services/modals/modal.service';
import { Router } from '@angular/router';
import { FirebaseService } from '../../services/firebase/firebase.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {

  avatarUrl: any;

  constructor(
    public modalService: ModalService,
    public firebaseService: FirebaseService,
    private router: Router,
  ) {
    this.modalService.showProfileModal = false;
    // tslint:disable-next-line: no-unused-expression
    this.modalService.mainPage = false;
  }

  ngOnInit(): void {
  }

  setAvatar(event: any): void {
    const file = event.target.files[0];
    if (!file.type.match(/image/) && file.size > 10000) { return; }
    const reader = new FileReader();
    reader.onload = (event) => {
      const avatarData = {
        avatarUrl: event.target!.result
      };
      this.firebaseService.updateUserData(avatarData);
    };
    reader.readAsDataURL(file);
  }

}
