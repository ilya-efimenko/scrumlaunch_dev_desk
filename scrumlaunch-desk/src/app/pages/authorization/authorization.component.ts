import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ModalService } from '../../services/modals/modal.service';
import { FirebaseService } from '../../services/firebase/firebase.service';


@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})
export class AuthorizationComponent implements OnInit {

  errorText: any;
  users: any;

  userForm: FormGroup = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(
    public formBuilder: FormBuilder,
    public modalService: ModalService,
    public firebaseService: FirebaseService
  ) {
    this.modalService.showProfileModal = false;
    this.modalService.showCreateDeskModal = false;
    this.modalService.showCreateTeamModal = false;
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.userForm = this.formBuilder.group({
      email: [localStorage.getItem('email'), Validators.compose([Validators.required, this.validInputs()])],
      password: ['', Validators.compose([Validators.required, this.validInputs()])]
    }, { updateOn: 'change' });
  }

  onSubmit(userData: any): void {
    this.firebaseService.signIn(userData);
    localStorage.setItem('userEmail', userData.email);
  }

  // tslint:disable-next-line: typedef
  validInputs() {
    return (control: FormControl) => {
      const regExpEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      const regExpPassword = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
      if (regExpEmail.test(control.value) || regExpPassword.test(control.value)) {
        return null;
      } else {
        return this.errorText = {
          restrictedTextEmail: 'Email is invalid.',
          restrictedTextPassword: 'Password is invalid.'
        };
      }
    };
  }
}
