import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FirebaseService } from '../../services/firebase/firebase.service';

@Component({
  selector: 'app-registration-second',
  templateUrl: './registration-second.component.html',
  styleUrls: ['./registration-second.component.scss']
})
export class RegistrationSecondComponent implements OnInit {

  @ViewChild('arrowSvg')
  arrowSvg!: ElementRef;

  @ViewChild('arrowSvgTypes')
  arrowSvgTypes!: ElementRef;

  constructor(
    public firebaseService: FirebaseService
  ) { }

  showTeamList = false;
  showDevList = false;
  showChosenTeam = 0;
  showChosenType = 0;

  userFormSecondData = {
    team: '',
    developerType: ''
  };

  pathToTop = `<path d="M12.707 7.293l7.071 7.071a1 1 0 01-1.414 1.414L12 9.414l-6.364 6.364a1 1 0 01-1.414-1.414l7.07-7.071a1 1 0 011.415 0z" fill="currentColor"></path>`;
  pathToBottom = `<path d="M11.293 16.707l-7.071-7.07a1 1 0 111.414-1.415L12 14.586l6.364-6.364a1 1 0 111.414 1.414l-7.07 7.071a1 1 0 01-1.415 0z" fill="currentColor"></path>`;

  teams = [] as any;

  types = [
    {
      typeName: 'Front-end developer'
    },
    {
      typeName: 'Back-end developer'
    },
    {
      typeName: 'IOS developer'
    },
    {
      typeName: 'Fullstack developer'
    }
  ];

  ngOnInit(): void {
  }

  toggleIcon(): void {
    this.showTeamList = !this.showTeamList;
    this.arrowSvg.nativeElement.innerHTML = this.showTeamList ? this.pathToTop : this.pathToBottom;
  }

  toggleTypesIcon(): void {
    this.showDevList = !this.showDevList;
    this.arrowSvgTypes.nativeElement.innerHTML = this.showDevList ? this.pathToTop : this.pathToBottom;
  }

  getDataFromUserSide(event: any): void {
    if (event.target.classList.contains('type_list')) {
      this.showChosenType = 1;
      this.userFormSecondData.developerType = event.target.innerText;
      this.toggleTypesIcon();
    } else {
      this.userFormSecondData.team = event.target.innerText;
      this.showChosenTeam = 1;
      this.toggleIcon();
    }
  }

}
