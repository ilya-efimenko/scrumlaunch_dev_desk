import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../services/modals/modal.service';
import { Router } from '@angular/router';
import { FirebaseService } from '../../services/firebase/firebase.service';

@Component({
  selector: 'app-developer-page',
  templateUrl: './developer-page.component.html',
  styleUrls: ['./developer-page.component.scss']
})
export class DeveloperPageComponent implements OnInit {


  constructor(
    public modalService: ModalService,
    public firebaseService: FirebaseService,
    private router: Router,
  ) {
    this.getUidFromRouterUrl();
    this.modalService.showProfileModal = false;
    // tslint:disable-next-line: no-unused-expression
    this.modalService.mainPage = false;
  }

  ngOnInit(): void {
    this.firebaseService.dataForDevPage.desks = [];
  }

  getUidFromRouterUrl(): void {
    const userUid = this.router.url.split('').reverse().join('').split('', 28).reverse().join('');
    this.firebaseService.getUserByUid(userUid);
  }

  goToDesk(id: string): void {
    this.router.navigate([`/desk/${id}`]);
  }

}
