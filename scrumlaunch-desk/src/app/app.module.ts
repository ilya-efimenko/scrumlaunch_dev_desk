import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { FormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCardModule } from '@angular/material/card';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatDialogModule, MatDialogRef, MatDialogState, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegistrationComponent } from './pages/registration/registration.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { AuthorizationComponent } from './pages/authorization/authorization.component';
import { HeaderComponent } from './components/header/header.component';
import { ProfileModalComponent } from './components/modals/profile-modal/profile-modal.component';
import { CreateDeskModalComponent } from './components/modals/create-desk-modal/create-desk-modal.component';
import { DeskPageComponent } from './pages/desk-page/desk-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegistrationSecondComponent } from './pages/registration-second/registration-second.component';
import { LeftNavbarComponent } from './components/left-navbar/left-navbar.component';
import { TicketComponent } from './components/ticket/ticket.component';
import { CreateTicketModalComponent } from './components/modals/create-ticket-modal/create-ticket-modal.component';
import { SearchModalComponent } from './components/modals/search-modal/search-modal.component';
import { DeveloperPageComponent } from './pages/developer-page/developer-page.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { LoginGuard } from './guards/login/login.guard';
import { CreateTeamModalComponent } from './components/modals/create-team-modal/create-team-modal.component';
import { TeamsPageComponent } from './pages/teams-page/teams-page.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    MainPageComponent,
    AuthorizationComponent,
    HeaderComponent,
    ProfileModalComponent,
    CreateDeskModalComponent,
    DeskPageComponent,
    RegistrationSecondComponent,
    LeftNavbarComponent,
    TicketComponent,
    CreateTicketModalComponent,
    SearchModalComponent,
    DeveloperPageComponent,
    ProfilePageComponent,
    CreateTeamModalComponent,
    TeamsPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    MatProgressBarModule,
    BrowserAnimationsModule,
    MatCardModule,
    DragDropModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule
  ],
  providers: [
    FormBuilder,
    LoginGuard,
    { provide: MatDialogRef, useValue: {} },
    // { provide: MAT_DIALOG_DATA, useValue: {} }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
