import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import firebase from 'firebase/app';
import 'firebase/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(
    public firebaseAuth: AngularFireAuth,
    private router: Router,
    public firestore: AngularFirestore,
    public firebaseDatabase: AngularFireDatabase
  ) {
    this.getCurrentUser();
    this.getAllUsers();
    this.getTeams();
  }

  showErrorText: boolean | undefined;
  errorMessage: any;

  userRef: AngularFirestoreDocument | undefined;
  currentUser: any;
  avatarData: any;
  usersAll: any;
  deskData = {
    deskId: this.router.url.split('').reverse().join('').split('', 9).reverse().join(''),
    title: '',
    backgroundUrl: ''
  };

  data = {
    uid: '',
    email: '',
    displayName: '',
    avatarUrl: '',
    developerType: '',
    team: '',
    userDesks: [] as any
  };

  dataForDevPage = {
    uid: '',
    email: '',
    displayName: '',
    avatarUrl: '',
    developerType: '',
    team: '',
    desks: [] as any
  };

  teams = [] as any;
  loading = true;

  signIn(userData: any): void {
    this.firebaseAuth.signInWithEmailAndPassword(userData.email, userData.password)
      .then((userCredential: any) => {
        this.router.navigate(['/main']);
      })
      .catch((error) => {
        console.log(error.message);
      });
  }

  signUp(userData: any): void {
    this.firebaseAuth.createUserWithEmailAndPassword(userData.email, userData.password)
      .then((userCredential: any) => {
        const userCred = Object.assign({}, userCredential.user, {
          displayName: `${userData.firstName} ${userData.lastName}`,
          avatarUrl: ''
        });
        this.updateUserData(userCred);
        this.router.navigate(['/registration/2']);
      })
      .catch((error) => {
        this.showErrorText = true;
        this.errorMessage = error.message;
      });
  }

  signUpSecond(userData: any): void {
    this.firebaseAuth.onAuthStateChanged((user: any) => {
      if (user) {
        if ((userData.developerType && userData.team).length) {
          this.updateUserData(userData);
          this.router.navigate(['/main']);
        }
      }
    });
  }

  async googleSignin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    const credential = await this.firebaseAuth.signInWithPopup(provider);
    this.router.navigate(['/main']);
    return this.updateUserData(credential.user);
  }

  logOut(): void {
    this.firebaseAuth.signOut().then(() => {
      this.data.userDesks = [];
      this.router.navigate(['/registration']);
    }).catch((error) => {
    });
    localStorage.removeItem('userEmail');
  }

  updateUserData(user: any) {
    // Sets user data to firestore on login
    this.userRef = this.firestore.doc(`users/${user.uid}`);

    if (this.router.url === '/registration/2') {
      const userRef = this.firestore.doc(`users/${this.currentUser.uid}`);
      const userSecondData = {
        developerType: user.developerType,
        team: user.team
      };
      return userRef.set(userSecondData, { merge: true });
    } else if (this.router.url === '/profile') {
      const userRef = this.firestore.doc(`users/${this.currentUser.uid}`);
      this.avatarData = user;
      return userRef.set(this.avatarData, { merge: true });
    } else {
      this.data.uid = user.uid;
      this.data.email = user.email;
      this.data.displayName = user.displayName;
      this.data.avatarUrl = user.avatarUrl;
      return this.userRef.set(this.data, { merge: true });
    }
  }

  getCurrentUser(): void {
    this.firebaseAuth.authState.subscribe((user: any) => {
      user && (this.currentUser = user);
    });
  }

  getAllUsers(): void {
    this.firestore.collection('users').valueChanges().subscribe((resp: any) => {
      this.usersAll = resp;
      this.setCurrentUserData(this.usersAll);
      this.loading = false;
    });
  }

  setCurrentUserData(allUsers: any): void {
    allUsers.forEach((user: any) => {
      if ((this.router.url !== '/main') && user.desks) {
        this.checkUserDeskByDeskId(user.desks, this.deskData.deskId);
      }
      if (user.uid === this.currentUser.uid) {
        this.data.displayName = user.displayName;
        this.data.developerType = user.developerType;
        this.data.team = user.team;
        this.data.email = user.email;
        this.data.avatarUrl = user.avatarUrl;
        if (user.desks) {
          this.data.userDesks = user.desks;
          this.checkUserDesk(this.deskData.deskId);
        }
      }
    });
  }

  getUserByUid(uid: any): void {
    this.firestore.collection('users').doc(uid).valueChanges().subscribe((userData: any) => {
      this.setCurrentDeveloperData(userData);
      this.loading = false;
    });
  }

  setCurrentDeveloperData(userData: any): void {
    if (userData.desks) {
      this.dataForDevPage.desks = userData.desks;
      this.checkUserDesk(this.deskData.deskId);
    }
    this.dataForDevPage.uid = userData.uid;
    this.dataForDevPage.displayName = userData.displayName;
    this.dataForDevPage.email = userData.email;
    this.dataForDevPage.avatarUrl = userData.avatarUrl;
    this.dataForDevPage.developerType = userData.developerType;
    this.dataForDevPage.team = userData.team;
  }

  updateUserDesks(body: any) {
    const userRef = this.firestore.doc(`users/${this.currentUser.uid}`);
    const desksData = {
      title: body.title,
      backgroundUrl: body.backgroundUrl,
      id: body.id
    };
    if (this.data.userDesks) {
      this.data.userDesks.push(desksData);
      return userRef.set({ desks: this.data.userDesks }, { merge: true });
    } else {
      return userRef.set({ desks: [desksData] }, { merge: true });
    }
  }

  checkUserDesk(deskId: any): void {
    this.checkUserDeskByDeskId(this.data.userDesks, deskId);
    this.checkUserDeskByDeskId(this.dataForDevPage.desks, deskId);
  }

  checkUserDeskByDeskId(desks: any, deskId: any): void {
    desks.forEach((desk: any) => {
      if (desk.id === deskId) {
        this.deskData.title = desk.title;
        this.deskData.backgroundUrl = desk.backgroundUrl;
      }
    });
  }

  getTeams(): void {
    this.firestore.collection('teams').valueChanges().subscribe((teams: any) => {
      this.teams = teams;
      this.filterUserByTeam(this.teams, this.usersAll);
      this.loading = false;
    });
  }

  setNewTeam(teamName: any) {
    this.router.navigate(['/teams']);
    const teamRef = this.firestore.collection('teams');
    return teamRef.add({ name: teamName });
  }

  filterUserByTeam(teams: any, users: any): void {
    users.find((user: any) => {
      teams.filter((team: any) => {
        if (user.team === team.name) {
          team.developers = [user];
          this.teams = teams;
        }
      });
    });
  }
}
