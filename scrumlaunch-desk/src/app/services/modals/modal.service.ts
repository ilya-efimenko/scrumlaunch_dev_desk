import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ModalService {

  showProfileModal = false;
  showCreateDeskModal = false;
  showCreateTicketModal = false;
  showSearchModal = false;
  showCreateTeamModal = false;
  mainPage = false;
  teamsPage = true;

  constructor() { }

  toggleShowingModal(): void {
    this.showProfileModal = !this.showProfileModal;
  }
}
