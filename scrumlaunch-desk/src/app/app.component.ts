import { Component } from '@angular/core';
import { ModalService } from './services/modals/modal.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'scrumlaunch-desk';

  constructor(
    public modalService: ModalService
  ) { }
}
