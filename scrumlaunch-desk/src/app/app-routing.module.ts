import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrationComponent } from './pages/registration/registration.component';
import { RegistrationSecondComponent } from './pages/registration-second/registration-second.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { AuthorizationComponent } from './pages/authorization/authorization.component';
import { DeskPageComponent } from './pages/desk-page/desk-page.component';
import { LoginGuard } from './guards/login/login.guard';
import { DeveloperPageComponent } from './pages/developer-page/developer-page.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { TeamsPageComponent } from './pages/teams-page/teams-page.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/authorization',
    pathMatch: 'full'
  },
  {
    path: 'authorization',
    component: AuthorizationComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent
  },
  {
    path: 'main',
    component: MainPageComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'registration/2',
    component: RegistrationSecondComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'desk/:id',
    component: DeskPageComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'profile',
    component: ProfilePageComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'developer/:id',
    component: DeveloperPageComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'teams',
    component: TeamsPageComponent,
    canActivate: [LoginGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
