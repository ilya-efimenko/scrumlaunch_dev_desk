import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Ticket } from '../../ticket/ticket';
import { ModalService } from '../../../services/modals/modal.service';

@Component({
  selector: 'app-create-ticket-modal',
  templateUrl: './create-ticket-modal.component.html',
  styleUrls: ['./create-ticket-modal.component.scss']
})
export class CreateTicketModalComponent implements OnInit {
  private backupTask: Partial<Ticket> = { ...this.data.ticket };

  constructor(
    public dialogRef: MatDialogRef<CreateTicketModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TicketDialogData,
    public modalService: ModalService
  ) { }

  ngOnInit(): void {
  }

  cancel(): void {
    this.data.ticket.title = this.backupTask.title;
    this.data.ticket.description = this.backupTask.description;
    this.dialogRef.close(this.data);
  }
}

export interface TicketDialogData {
  ticket: Partial<Ticket>;
  enableDelete: boolean;
}

export interface TicketDialogResult {
  ticket: Ticket;
  delete?: boolean;
}
