import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDeskModalComponent } from './create-desk-modal.component';

describe('CreateDeskModalComponent', () => {
  let component: CreateDeskModalComponent;
  let fixture: ComponentFixture<CreateDeskModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateDeskModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDeskModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
