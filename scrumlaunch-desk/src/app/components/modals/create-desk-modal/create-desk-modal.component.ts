import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ModalService } from '../../../services/modals/modal.service';
import { FirebaseService } from '../../../services/firebase/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-desk-modal',
  templateUrl: './create-desk-modal.component.html',
  styleUrls: ['./create-desk-modal.component.scss']
})
export class CreateDeskModalComponent implements OnInit {

  @ViewChild('backgroundElement')
  backgroundElement!: ElementRef;
  @ViewChild('inputElementDeskTitle')
  inputElementDeskTitle!: ElementRef;
  title: any;
  backgroundPhotos = [
    {
      backgroundUrl: '/assets/mountains.jpeg'
    },
    {
      backgroundUrl: '/assets/brown_mountains.jpeg'
    },
    {
      backgroundUrl: '/assets/palms.jpeg'
    },
    {
      backgroundUrl: '/assets/car.jpeg'
    },
    {
      backgroundUrl: '/assets/blue_colors.jpeg'
    },
    {
      backgroundUrl: '/assets/tunnel.jpeg'
    },
    {
      backgroundUrl: '/assets/white_back.jpeg'
    },
    {
      backgroundUrl: '/assets/ice.jpeg'
    },
    {
      backgroundUrl: '/assets/mix_colors.jpeg'
    }
  ];

  res: any;

  constructor(
    public modalService: ModalService,
    public firebaseService: FirebaseService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  setBackground(event: any): void {
    this.backgroundElement.nativeElement.style.backgroundImage = event.target.style.backgroundImage;
  }

  createDesk(): void {
    const body = {
      title: this.inputElementDeskTitle.nativeElement.value,
      backgroundUrl: this.backgroundElement.nativeElement.style.backgroundImage,
      id: Math.random().toString(36).substr(2, 9)
    };
    const id = body.id;
    this.firebaseService.updateUserDesks(body);
    this.router.navigate([`/desk/${id}`]);
  }
}

