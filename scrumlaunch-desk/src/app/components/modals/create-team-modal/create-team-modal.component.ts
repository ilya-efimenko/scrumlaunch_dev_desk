import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalService } from '../../../services/modals/modal.service';
import { FirebaseService } from '../../../services/firebase/firebase.service';
@Component({
  selector: 'app-create-team-modal',
  templateUrl: './create-team-modal.component.html',
  styleUrls: ['./create-team-modal.component.scss']
})
export class CreateTeamModalComponent implements OnInit {

  name: string | undefined;
  @ViewChild('teamNameInput')
  teamNameInput!: ElementRef;

  constructor(
    public modalService: ModalService,
    public firebaseService: FirebaseService
  ) { }

  ngOnInit(): void {
  }

}
