import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../../services/modals/modal.service';
import { FirebaseService } from '../../../services/firebase/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-modal',
  templateUrl: './profile-modal.component.html',
  styleUrls: ['./profile-modal.component.scss']
})
export class ProfileModalComponent implements OnInit {

  avatarUrl: any;

  constructor(
    public modalService: ModalService,
    public router: Router,
    public firebaseService: FirebaseService
  ) { }

  ngOnInit(): void {
  }
}
