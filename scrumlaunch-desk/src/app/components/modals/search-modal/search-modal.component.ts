import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ModalService } from '../../../services/modals/modal.service';
import { FirebaseService } from '../../../services/firebase/firebase.service';

@Component({
  selector: 'app-search-modal',
  templateUrl: './search-modal.component.html',
  styleUrls: ['./search-modal.component.scss']
})
export class SearchModalComponent implements OnInit {


  developersToShow = [] as any;

  constructor(
    public modalService: ModalService,
    public firebaseService: FirebaseService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  onKey(event: any): void {
    if (!event.target.value.length) {
      this.developersToShow = [];
      return;
    }
    this.developersToShow = this.firebaseService.usersAll.filter((developer: any) => {
      // tslint:disable-next-line: no-unused-expression
      return developer.displayName && developer.displayName.split(' ').some((word: any) => {
        return event.target.value.toLowerCase().split(' ').some((eventWord: any) => {
          if (word.toLowerCase().includes(eventWord) && eventWord.length) {
            return true;
          }
        });
      });
    });
  }

  goToDeveloper(id: any): void {
    this.router.navigate([`/developer/${id}`]);
  }
}
