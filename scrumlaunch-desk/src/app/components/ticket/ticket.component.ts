import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Ticket } from './ticket';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {

  @Input() deskId: any;
  @Input() ticket!: Ticket;
  @Output() edit = new EventEmitter<Ticket>();

  constructor() { }

  ngOnInit(): void {
  }

}
