import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ModalService } from '../../services/modals/modal.service';
import { FirebaseService } from '../../services/firebase/firebase.service';

@Component({
  selector: 'app-left-navbar',
  templateUrl: './left-navbar.component.html',
  styleUrls: ['./left-navbar.component.scss']
})
export class LeftNavbarComponent implements OnInit {

  @ViewChild('arrowSvg')
  arrowSvg!: ElementRef;

  showList = false;
  pathToTop = `<path d="M12.707 7.293l7.071 7.071a1 1 0 01-1.414 1.414L12 9.414l-6.364 6.364a1 1 0 01-1.414-1.414l7.07-7.071a1 1 0 011.415 0z" fill="currentColor"></path>`;
  pathToBottom = `<path d="M11.293 16.707l-7.071-7.07a1 1 0 111.414-1.415L12 14.586l6.364-6.364a1 1 0 111.414 1.414l-7.07 7.071a1 1 0 01-1.415 0z" fill="currentColor"></path>`;


  constructor(
    public modalService: ModalService,
    public firebaseService: FirebaseService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  toggleIcon(): void {
    this.showList = !this.showList;
    // tslint:disable-next-line: no-unused-expression
    this.arrowSvg.nativeElement.innerHTML = this.showList ? this.pathToTop : this.pathToBottom;
  }

  goToDeveloper(id: any): void {
    this.router.navigate([`/developer/${id}`]);
  }
}
