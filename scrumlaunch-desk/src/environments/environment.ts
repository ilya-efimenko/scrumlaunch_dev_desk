// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBBJogCMsqwvGR8bh1QVBXMQpUj0nTHvJk",
    authDomain: "scrumlaunch-desk.firebaseapp.com",
    projectId: "scrumlaunch-desk",
    storageBucket: "scrumlaunch-desk.appspot.com",
    messagingSenderId: "141459382430",
    appId: "1:141459382430:web:4bc1673b6e2c0b3ba01b8f",
    measurementId: "G-JN5SS5YYYM"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
